package training.mailru.api;

import io.qameta.allure.*;
import io.restassured.http.Cookies;
import io.restassured.response.ExtractableResponse;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import training.mailru.TestBase;

import static io.qameta.allure.Allure.step;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Epic("Mail.ru Тесты")
@Feature("API")
public class ApiLoginTest extends TestBase {

    private final static String mainHost = "mail.ru", protocol = "https://",
            mainPage = protocol + mainHost,
            authURI = protocol + "auth." + mainHost;

    @Story("Аутентификация на сайте через API")
    @Description(value = "Аутентификация на сайте через API")
    @Severity(value = SeverityLevel.CRITICAL)
    @DisplayName("Аутентификация на сайте через API")
    @Test
    public void loginViaApi() {

        Cookies cookies = step("1. Подготовка данных для входа", () -> {
            ExtractableResponse<Response> response =
                    step("1.1 Запросить стартовую страницу", () ->
                            given()
                                    .expect()
                                    .statusCode(200)
                                    .when()
                                    .get(mainPage)
                                    .then().extract());
            step("1.2 Проверить, что вход еще не произведен", () ->
                    assertFalse(response.body().asString().contains(login)));

            return response.detailedCookies();
        });

        Cookies authCookies = step("2. Аутентификация", () -> {
            ValidatableResponse response =
                    step("2.1 Осуществить аутентификацию", () ->
                            given()
                                    .cookies(cookies)
                                    .header("Referer", mainPage)
                                    .contentType("application/x-www-form-urlencoded")
                                    .param("login", login)
                                    .param("password", password)
                                    .param("saveauth", "1")
                                    .param("token", cookies.getValues("act"))
                                    .param("project", "e.mail.ru")
                                    .expect()
                                    .statusCode(200)
                                    .when()
                                    .post(authURI +"/jsapi/auth").then().log().all());
            step("2.2 Проверить успешность выполнения запроса", () ->
                    response.body("status", equalTo("ok")));

            return response.extract().detailedCookies();
        });

        step("3. Проверка успещности авторизации", () -> {
            ExtractableResponse<Response> response =
                    step("3.1 Запросить стартовую страницу", () ->
                            given()
                                    .cookies(cookies)
                                    .cookies(authCookies)
                                    .expect()
                                    .statusCode(200)
                                    .when()
                                    .get(mainPage)
                                    .then().extract());
            step("3.2 Проверить, что вход произведен", () ->
                    assertTrue(response.body().asString().contains(login)));
        });

    }

}