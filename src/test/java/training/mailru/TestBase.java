package training.mailru;

import org.junit.jupiter.api.BeforeAll;

import java.io.FileInputStream;
import java.util.Properties;

public class TestBase {

    private static final String credentialsPath = "src/test/resources/credentials.properties";
    protected static String login, password;

    @BeforeAll
    private static void loadCredentials() {
        try (FileInputStream credentials = new FileInputStream(credentialsPath)) {
            Properties prop = new Properties();
            prop.load(credentials);
            login = prop.getProperty("login");
            password = prop.getProperty("password");
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.exit(1);
        }
    }
}
