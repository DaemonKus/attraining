package training.mailru.ui;

import io.qameta.allure.*;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import training.mailru.TestBase;

import static io.qameta.allure.Allure.step;
import static training.selenium.LikeSelenide.*;
import static training.selenium.condition.LikeCondition.*;

@Epic("Mail.ru Тесты")
@Feature("UI")
public class UILoginTest extends TestBase {

    @BeforeAll
    private static void init() {
        System.setProperty("webdriver.chrome.driver", "./src/test/resources/chromedriver.exe");
        ChromeOptions localDriverOptions = new ChromeOptions();
        localDriverOptions.addArguments("--disable-notifications"); //Disable Web push notifications in Chrome
        localDriverOptions.addArguments("--ignore-certificate-errors");
        localDriverOptions.addArguments("start-maximized");
        setDriver(new ChromeDriver(localDriverOptions));
    }

    @AfterAll
    private static void end() {
        close();
    }

    @Story("Аутентификация на сайте через UI")
    @Description(value = "Аутентификация на сайте через UI")
    @Severity(value = SeverityLevel.CRITICAL)
    @DisplayName("Аутентификация на сайте через UI")
    @Test
    public void uiLogin() {

        step("1. Открытие страницы mail.ru", () -> open("https://mail.ru"));
        step("2. Вход в почту", () -> {
            step("2.1 Ввести имя ящика в текстовое поле", () ->
                    $(By.name("login")).setValue(login));
            step("2.2 Нажать кнопку 'Ввести пароль'", () ->
                    $("button.button.svelte-no02r").should(enabled).click());
            step("2.3 Ввести пароль в текстовое поле", () ->
                    $(By.name("password")).setValue(password));
            step("2.4 Нажать кнопку 'Войти'", () ->
                $("button.second-button.svelte-no02r").should(enabled).click());
            step("2.5 Проверить отображение логина", () ->
                    $(By.id("PH_user-email"))
                            .waitUntil(visible, 10_000)
                            .should(text(login)));
        });
    }

    @AfterEach
    private void afterTest() {
        clearCookies();
    }
}
