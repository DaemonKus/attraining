package training.selenium.condition;

import training.selenium.LikeSelenideElement;

import java.util.Arrays;

/**
 * Возможные условия для элементов
 */
public interface LikeCondition {
    /*
    Видимость элемента
     */
    LikeCondition visible = LikeSelenideElement::isDisplayed,
            /*
            Отсутствие введенного текста
             */
            empty = element -> element.getText().isEmpty(),
            /*
            Активность
             */
            enabled = LikeSelenideElement::isEnabled;

    /**
     * Проверка соответствия элемента условию
     * @param element проверяемый элемент
     * @return true, если соответствует, false - если нет
     */
    boolean match(LikeSelenideElement element);

    /**
     * Проверка соответствия текста элемента
     * @param text проверяемый текст
     * @return объект условия
     */
    static LikeCondition text(String text) {
        return element ->
                element
                        .getText()
                        .equals(text);
    }

    /**
     * Проверка соответствия элемента обратному условию
     * @param condition изначальное условие
     * @return инвертированное условие
     */
    static LikeCondition not(LikeCondition condition) {
        return element -> !condition.match(element);
    }

    /**
     * Проверка соответствия элемента хотя бы одному из условий
     * @param conditions проверяемые условия
     * @return объект условия
     */
    static LikeCondition or(LikeCondition... conditions) {
        return element -> Arrays.stream(conditions)
                .map(condition -> condition.match(element))
                .reduce((cur, next) -> cur | next)
                .orElse(false);
    }

    /**
     * Проверка соответствия элемента всем условиям
     * @param conditions проверяемые условия
     * @return объект условия
     */
    static LikeCondition and(LikeCondition... conditions) {
        return element -> Arrays.stream(conditions)
                .map(condition -> condition.match(element))
                .reduce((cur, next) -> cur & next)
                .orElse(false);
    }

}
