package training.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static java.time.Duration.ofMillis;

/**
 * Класс для работы с Selenium
 */
public class LikeSelenide {

    /*
    Веб-драйвер от Selenium
     */
    static WebDriver driver = null;
    /*
    Веб-драйвер для явных ожиданий
     */
    static WebDriverWait driverWait = null;

    /*
    Время неявного ожидания
     */
    private static int defaultDelay = 4_000;

    /**
     * Инициализация веб-драйверов
     * @param driver веб-драйвер, используемый Selenium
     */
    public static void setDriver(WebDriver driver) {
        if (LikeSelenide.driver == null) {
            LikeSelenide.driver = driver;
            driver.manage().timeouts()
                    .implicitlyWait(defaultDelay, TimeUnit.MILLISECONDS);
            driverWait = new WebDriverWait(driver, 2 * defaultDelay / 1000);
        }
    }

    /**
     * Изменение неявного ожидания
     * @param delayInMillis время в миллисекундах
     */
    public static void setDefaultDelay(int delayInMillis) {
        defaultDelay = delayInMillis;
        driver.manage().timeouts()
                .implicitlyWait(defaultDelay, TimeUnit.MILLISECONDS);
        driverWait.withTimeout(Duration.ofMillis(2 * defaultDelay));
    }

    /**
     * Получить текущее время неявного ожидания
     * @return текущее время неявного ожидания в мс
     */
    public static int getDefaultDelay() {
        return defaultDelay;
    }

    /**
     * Проверка, загружена ли страница
     * @param delayInMillis время ожидания загрузки страницы в мс
     * @return true, если страница загрузилась за указанное время;
     * false - если нет
     */
    public static boolean pageLoaded(int delayInMillis) {
        return driverWait
                .withTimeout(ofMillis(delayInMillis))
                .until(driver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete"));
    }

    /**
     * Поиск элемента на странице (сам поиск не запускается) по селектору
     * @param selector селектор для поиска
     * @return объект LikeSelenideElement, соответствующий заданному селектору
     */
    public static LikeSelenideElement $(By selector) {
        return new LikeSelenideElement(selector);
    }

    /**
     * Поиск элемента на странице (сам поиск не запускается) по xpath
     * @param xpathExpression xpath элемента
     * @return объект LikeSelenideElement, соответствующий заданному xpath
     */
    public static LikeSelenideElement $x(String xpathExpression) {
        return $(By.xpath(xpathExpression));
    }

    /**
     * Поиск элемента на странице (сам поиск не запускается) по css-селектору
     * @param cssSelector css-селектор для поиска
     * @return объект LikeSelenideElement, соответствующий заданному css-селектору
     */
    public static LikeSelenideElement $(String cssSelector) {
        return $(By.cssSelector(cssSelector));
    }

    /**
     * Запуск веб-драйвера и открытие в нем страницы
     * @param url URL открываемой страницы
     */
    public static void open(String url) {
        driver.get(url);
    }

    /**
     * Завершение работы веб-драйвера
     */
    public static void close() {
        driver.close();
        driver.quit();
    }

    /**
     * Очистка cookies
     */
    public static void clearCookies() {
        driver.manage().deleteAllCookies();
    }

}
