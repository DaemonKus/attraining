package training.selenium;

import org.openqa.selenium.*;
import training.selenium.condition.LikeCondition;

import java.util.List;

import static java.time.Duration.ofMillis;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfNestedElementLocatedBy;
import static training.selenium.LikeSelenide.*;

/**
 * Класс для работы с веб-элементами
 */
public class LikeSelenideElement implements WebElement {

    /*
    Элемент-родитель
     */
    private final LikeSelenideElement parentElement;
    /*
    Селектор для поиска
     */
    private final By currentSelector;

    /**
     * Конструктор
     * @param selector селектор необходимого элемента
     */
    public LikeSelenideElement(By selector) {
        currentSelector = selector;
        parentElement = null;
    }

    /**
     * Конструктор для последовательного поиска
     * @param selector селектор необходимого элемента
     * @param fromElement элемент, от которого начинается поиск
     */
    private LikeSelenideElement(By selector, LikeSelenideElement fromElement) {
        currentSelector = selector;
        parentElement = fromElement;
    }

    /**
     * Запуск поиска элемента
     * @return найденный Selenium WebElement
     */
    private WebElement reducedElement() {
        return parentElement == null ?
                driver.findElement(currentSelector) :
                parentElement.reducedElement().findElement(currentSelector);
    }

    /**
     * Клик по элементу
     */
    @Override
    public void click() {
        reducedElement().click();
    }

    /**
     * Отправка элемента, если это форма или её часть
     */
    @Override
    public void submit() {
        reducedElement().submit();
    }

    /**
     * Симуляция нажатия клавиш в элементе
     * @param keysToSend последовательность клавиши
     */
    @Override
    public void sendKeys(CharSequence... keysToSend) {
        reducedElement().sendKeys(keysToSend);
    }

    /**
     * Очистить введенный текст в элементе
     */
    @Override
    public void clear() {
        reducedElement().clear();
    }

    @Override
    public String getTagName() {
        return reducedElement().getTagName();
    }

    @Override
    public String getAttribute(String name) {
        return reducedElement().getCssValue(name);
    }

    @Override
    public boolean isSelected() {
        return reducedElement().isSelected();
    }

    /**
     * Проверка свойства enabled
     * @return true, если enabled=true, иначе - false
     */
    @Override
    public boolean isEnabled() {
        return reducedElement().isEnabled();
    }

    /**
     * Получает видимый текст элемента
     * @return видимый текст элемента
     */
    @Override
    public String getText() {
        return reducedElement().getText();
    }

    @Override
    public List<WebElement> findElements(By by) {
        return reducedElement().findElements(by);
    }

    @Override
    public WebElement findElement(By by) {
        return reducedElement().findElement(by);
    }

    /**
     * Проверка отображения элемента на странице
     * @return true, если отображается, false - если нет
     */
    @Override
    public boolean isDisplayed() {
        return reducedElement().isDisplayed();
    }

    @Override
    public Point getLocation() {
        return reducedElement().getLocation();
    }

    @Override
    public Dimension getSize() {
        return reducedElement().getSize();
    }

    @Override
    public Rectangle getRect() {
        return reducedElement().getRect();
    }

    @Override
    public String getCssValue(String propertyName) {
        return reducedElement().getCssValue(propertyName);
    }

    @Override
    public <X> X getScreenshotAs(OutputType<X> target) throws WebDriverException {
        return reducedElement().getScreenshotAs(target);
    }

    /**
     * Очистить введенный текст в элементе и ввести новый
     * @param value новый текст
     * @return текущий элемент
     */
    public LikeSelenideElement setValue(String value) {
        WebElement currentElement = reducedElement();
        currentElement.clear();
        currentElement.sendKeys(value);
        return this;
    }

    /**
     * Поиск элемента на странице (сам поиск не запускается) по селектору
     * @param selector селектор для поиска
     * @return новый объект LikeSelenideElement, соответствующий заданному селектору,
     * с установленным текущим элементом в качестве родительского
     */
    public LikeSelenideElement $(By selector) {
        return new LikeSelenideElement(selector, this);
    }

    /**
     * Поиск элемента на странице (сам поиск не запускается) по xpath
     * @param xpathExpression xpath элемента
     * @return объект LikeSelenideElement, соответствующий заданному xpath,
     * с установленным текущим элементом в качестве родительского
     */
    public LikeSelenideElement $x(String xpathExpression) {
        return new LikeSelenideElement(By.xpath(xpathExpression), this);
    }

    /**
     * Поиск элемента на странице (сам поиск не запускается) по css-селектору
     * @param cssSelector css-селектор для поиска
     * @return объект LikeSelenideElement, соответствующий заданному css-селектору,
     * с установленным текущим элементом в качестве родительского
     */
    public LikeSelenideElement $(String cssSelector) {
        return new LikeSelenideElement(By.cssSelector(cssSelector), this);
    }

    /**
     * Проверка, соответствует ли элемент условию с выходом при несоответствии
     * @param condition условие для соответствия
     * @return текущий элемент
     * @throws AssertionError при несоответствии
     */
    public LikeSelenideElement should(LikeCondition condition) {
        assert is(condition);
        return this;
    }

    /**
     * Проверка, соответствует ли элемент условию
     * @param condition условие для соответствия
     * @return true, если соответствует, false - если нет
     */
    public boolean is(LikeCondition condition) {
        return condition.match(this);
    }

    /**
     * Запуск поиска элемента с явным ожиданием
     * @param delayInMillis время явного ожидания в мс
     * @return найденный Selenium WebElement
     */
    private WebElement reducedElement(int delayInMillis) {
        while (!pageLoaded(delayInMillis));
        return parentElement == null ?
                driverWait
                        .withTimeout(ofMillis(delayInMillis))
                        .until(presenceOfElementLocated(currentSelector)) :
                driverWait
                        .withTimeout(ofMillis(delayInMillis))
                        .until(presenceOfNestedElementLocatedBy(
                                parentElement.reducedElement(delayInMillis),
                                currentSelector
                        ));
    }

    /**
     * Проверка соответствия элемента условию с явным ожиданием
     * @param condition условие для соответствия
     * @param delayInMillis время явного ожидания в мс
     * @return текущий элемент
     * @throws TimeoutException если условие так и не выполнилось за заданное время
     */
    public LikeSelenideElement waitUntil(LikeCondition condition, int delayInMillis) {
        driverWait.ignoring(StaleElementReferenceException.class);
        reducedElement(delayInMillis);
        driverWait
                .withTimeout(ofMillis(delayInMillis))
                .until(driver -> condition.match(this));
        return this;
    }
}
