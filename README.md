# ATTraining

## Проект для совершенствования навыков в AT

####Тесты Mail.ru
- [Аутентификация через API](./src/test/java/training/mailru/api/ApiLoginTest.java)
- [Аутентификация через UI](./src/test/java/training/mailru/ui/UILoginTest.java)

Для успешного входа необходимо ввести корректные данные аутентификации в [файл](./src/test/resources/credentials.properties)

####Мини-фреймворк [LikeSelenide](./src/main/java/training/selenium)
- Класс [LikeSelenide](./src/main/java/training/selenium/LikeSelenide.java)
- Класс веб-элемента [LikeSelenideElement](./src/main/java/training/selenium/LikeSelenideElement.java)
- Интерфейс условий [LikeCondition](./src/main/java/training/selenium/condition/LikeCondition.java)